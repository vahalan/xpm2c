﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XPM2C
{
    public partial class XPM2C : Form
    {
        System.IO.Stream myStream = null;
        ImageAsC imageAsC = new ImageAsC();

        // ImageConverter object used to convert byte arrays containing JPEG or PNG file images into 
        //  Bitmap objects. This is static and only gets instantiated once.
        private static readonly ImageConverter _imageConverter = new ImageConverter();

        public XPM2C()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedTab = tabPage1;

            openFileDialog1.Title = "Select a XPM File";
            openFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);
            openFileDialog1.Filter = "xpm files (*.xpm)|*.xpm|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = false;

            char[] trimChars = { '"', ',' };
            char[] splitChars = { ' ', '\t' };

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if ((myStream = openFileDialog1.OpenFile()) != null)
                {
                    progressBar1.Step = 1;
                    progressBar1.Maximum = 10;
                    progressBar1.Visible = true;

                    textBox1.Text = System.IO.File.ReadAllText(openFileDialog1.FileName);

                    for (int i = 0; i < textBox1.Lines.GetLength(0); i++)
                    {
                        if (i == 2)                                         // image info
                        {
                            imageAsC.AddImageInfo(textBox1.Lines[i]);
                            progressBar1.Maximum = imageAsC.height * 12 + textBox1.Lines.GetLength(0);
                            progressBar1.PerformStep();
                        }
                        else if (i > 2 && i < imageAsC.numColors+3)         // Color palette 
                        {
                            imageAsC.AddToPalette(textBox1.Lines[i]);
                            progressBar1.PerformStep();
                        }
                        else if (i > imageAsC.numColors+2)                  // image data starts after palette
                        {
                            imageAsC.AddToImageData(textBox1.Lines[i]);     // will read until the end of file
                            progressBar1.PerformStep();
                        }
                    }
                }

                // Image information
                List<string> ii = new List<string>();
                imageAsC.GenerateImageInfo(ref ii);

                foreach (string line in ii)
                    debugBox.AppendText(line);

                // Palette
                List<string> li = new List<string>();
                imageAsC.GeneratePaletteCode(ref li);

                foreach (string line in li)
                    debugBox.AppendText(line);

                // Image code
                List<string> code = new List<string>();
                imageAsC.GenerateImageCode(ref code);

                foreach (string line in code)
                {
                    progressBar1.PerformStep();
                    debugBox.AppendText(line);
                }

                // Packed image code
                List<string> pcode = new List<string>();
                imageAsC.GeneratePackedImageCode(ref pcode);

                foreach (string line in pcode)
                {
                    progressBar1.PerformStep();
                    debugBox.AppendText(line);
                }
                drawImage();
                drawPaletteR8G8B8(40);
                drawPaletteR5G6B5(40);

                progressBar1.Visible = false;
                tabControl1.SelectedTab = tabPage2;
            }
        }

        public void drawImage()
        {
            pictureBox1.Size = new Size(imageAsC.width, imageAsC.height);

            Bitmap image = new Bitmap(imageAsC.width, imageAsC.height);

            byte pix;
            Color c;

            for (int y = 0; y < imageAsC.height; y++)
            {
                progressBar1.PerformStep();

                for (int x = 0; x < imageAsC.width; x++)
                {
                    pix = imageAsC.imageData[y * imageAsC.width + x];
                    if (pix > 15)
                        pix = 15;
                    c = Color.FromArgb((int)imageAsC.palette_a8r8b8g8[pix]);

                    image.SetPixel(x, y, c);
                }
            }
            pictureBox1.Image = image;
        }

        public void drawPaletteR8G8B8(int h)
        {
            int w = Math.Min(pictureBox1.Width, this.Width - 10);

            pictureBox2.Size = new Size(w, h);

            Bitmap paletteImage = new Bitmap(w, h);

            Color c;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int colInd = (x * imageAsC.palette_a8r8b8g8.Count) / w;
                    c = Color.FromArgb((int)imageAsC.palette_a8r8b8g8[colInd]);
                    paletteImage.SetPixel(x, y, c);
                }
            }
            pictureBox2.Image = paletteImage;
        }

        public void drawPaletteR5G6B5(int h)
        {
            int w = Math.Min(pictureBox1.Width, this.Width-10);

            pictureBox3.Size = new Size(w, h);

            Bitmap paletteImage = new Bitmap(w, h);

            Color c;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int colInd = (x * imageAsC.colorList.Count) / w;
                    c = Color.FromArgb(
                        imageAsC.colorList[colInd].color.R & (0x0ff << 3),
                        imageAsC.colorList[colInd].color.G & (0x0ff << 2),
                        imageAsC.colorList[colInd].color.B & (0x0ff << 3));

                    paletteImage.SetPixel(x, y, c);
                }
            }
            pictureBox3.Image = paletteImage;
        }
    }
}
