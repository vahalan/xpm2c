﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Text.RegularExpressions;

public class ImageAsC
{
    public List<UInt32> palette_a8r8b8g8 = new List<UInt32>();
    public List<byte> imageData = new List<byte>();
    public List<ColorTable> colorList = new List<ColorTable>();

    ColorConverter converter = new ColorConverter();
    List<UInt16> palette_r5b6g5 = new List<UInt16>();

    char[] trimChars = { '"', ',' };
    char[] splitChars = { ' ', '\t' };

    public int width { get; private set; }
    public int height { get; private set; }
    public int numColors { get; private set; }
    public int charsPerPixel { get; private set; }

    public ImageAsC()
	{
        width = 0;
        height = 0;
        numColors = 0;
        charsPerPixel = 0;
	}

    public void AddImageInfo(string line)
    {
        string[] parts = line.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
        width          = Convert.ToInt32(parts[0].Trim(trimChars));
        height         = Convert.ToInt32(parts[1]);
        numColors      = Convert.ToInt32(parts[2]);
        charsPerPixel  = Convert.ToInt32(parts[3].TrimEnd(trimChars));
    }

    /// <summary>
    /// Adds to image data.
    /// </summary>
    /// <param name="line">The line containing one line of image data.</param>
    public void AddToImageData(string line)
    {
        if (charsPerPixel > 0 && colorList.Count > 0 && line.Length > 0)
        {
            string s = line;
            for (int j = 1; j <= charsPerPixel*width; j += charsPerPixel)
            {
                string id = s.Substring(j, charsPerPixel);
                byte b = (byte)colorList.IndexOf(colorList.Find(ct => ct.colorString == id));

                imageData.Add(b);
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="index"></param>
    /// <param name="hexCode"></param>
    /// 
    static string pattern = "\"(.{1,3})\\t([cmgCMG])\\s(#[a-fA-F0-9]{2,6}|None)\",";
    static Regex rgx = new Regex(pattern, 
        RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);

    public void AddToPalette(string line)
    {
        ColorTable ct;

        Match parts = rgx.Match(line);

        if (parts.Success && parts.Groups[2].Value == "c")      // c=color, other possibilies: m=monochrome, g=grayscale",
        {                                                       // s=symbolic. See https://en.wikipedia.org/wiki/X_PixMap
            //string a = parts[0].Trim(trimChars);
            //string b = parts[2].Trim(trimChars);
            string a = parts.Groups[1].Value;
            string b = parts.Groups[3].Value;

            if (parts.Groups[3].Value.ToLower() == "none")      // Transparent color
            {
                ct = new ColorTable(a, "#00FFFFFF");            // Transparent color: full transparency & white

                palette_a8r8b8g8.Add(0x00FFFFFF);
                palette_r5b6g5.Add(0xFFFF);
            }
            else                                                // 'Normal' Color
            {
                if (a == "")
                    a = ",";
                ct = new ColorTable(a, b);                      // 'Normal' Color

                Color col = ((Color)converter.ConvertFromString(b));
                UInt32 argb = (UInt32)col.ToArgb();
                palette_a8r8b8g8.Add(argb);

                byte alpha    = (byte)(argb >> 24);
                UInt16 rr     = (UInt16)(col.R >> 3);
                UInt16 gg     = (UInt16)(col.G >> 2);
                UInt16 bb     = (UInt16)(col.B >> 3);
                UInt16 r5g6b5 = (UInt16)(rr << 11| gg << 5 | bb);
                palette_r5b6g5.Add(r5g6b5);
            }
            colorList.Add(ct);
        }
    }

    /// <summary>
    /// Generates the image information.
    /// </summary>
    /// <param name="stringList">The string list.</param>
    public void GenerateImageInfo(ref List<string> stringList)
    {
        stringList.Add("uint16_t image_width  = " + width + ";\r\n");
        stringList.Add("uint16_t image_height = " + height + ";\r\n");
        stringList.Add("uint16_t image_colors = " + numColors + ";\r\n\r\n");
    }

    /// <summary>
    /// Generates the image code in a C lang table format.
    /// </summary>
    /// <param name="stringList">Reference of a string list to contain the definition.</param>
    public void GenerateImageCode(ref List<string> stringList)
    {
        string tmpstr = "";
        string row = "";

        int bytesPerRow = 32;                                               // this many bytes per row on output

        stringList.Add("uint8_t image[] = {\r\n");                          // start of table definition

        for (int y = 0; y < imageData.Count; y++)                           // go through the datalist
        {
            if (y % bytesPerRow == 0)                                       // need to do cr-lf?
            {
                if (row.Length > 0)                                         // dota on the row?
                {
                    if (y/bytesPerRow != imageData.Count/bytesPerRow)       // not the last line?
                        row += ",";                                         // add comma to end of line

                    stringList.Add(row + "\r\n");                           // add row to outut list
                }
                row = $"/* R:{y / width:D4} */ ";                           // new line 'header'
            }

            tmpstr = (y % bytesPerRow != 0)?", ":"";                        // add comma if not first byte of the line

            row += tmpstr + $"0x{imageData[y]:X2}";
        }

        if (row.Length > 0)
            stringList.Add(row+"\r\n");

        stringList.Add("}\r\n");
    }

    /// <summary>
    /// Generates the packed image code.
    /// </summary>
    /// <param name="stringList">The string list.</param>
    public void GeneratePackedImageCode(ref List<string> stringList)
    {
        if (numColors > 16)
        {
            stringList.Add("\r\n// uint8_t image_packed[] = {};\r\n");
            stringList.Add("// Packing data to two pixels per byte is only possible if palette\r\n");
            stringList.Add("// has max 16 colors. This image has " + numColors + " colors\r\n");
        }
        else
        {
            string tmpstr = "";
            string row = "";
            byte h = 0;
            uint l = 0;

            int bytesPerRow = 64;

            stringList.Add("uint8_t image_packed[] = {\r\n");

            for (int y = 0; y < imageData.Count; y += 2)
            {
                tmpstr = (y % bytesPerRow != 0) ? ", " : "";

                l = (byte)((imageData[y]) & 0x0F);
                if ((y + 1) >= imageData.Count) h = 0; else h = (byte)((imageData[y + 1] & 0x0F) << 4);

                if (y % bytesPerRow == 0)                                           // need to do cr-lf?
                {
                    if (row.Length > 0)                                             // have something on the row?
                    {
                        if (y / bytesPerRow != imageData.Count / bytesPerRow)       // not the last line?
                            row += ",";

                        stringList.Add(row + "\r\n");                               // add the row to list
                    }
                    row = $"/* R:{y / width:D4} */ ";
                }

                row += tmpstr + $"0x{(h | l):X2}";                                  // add current byte to row string
            }

            if (row.Length > 0)
                stringList.Add(row + "\r\n");

            stringList.Add("};\r\n");
        }
    }

    public void GeneratePaletteCode(ref List<string> stringList)
    {
        string tmpstr = "";

        stringList.Add("uint32_t palette_a8r8b8g8[" + palette_a8r8b8g8.Count + "] = \r\n{");

        for (int i = 0; i < palette_a8r8b8g8.Count; i++)
        {
            tmpstr = (i < palette_a8r8b8g8.Count - 1) ? "," : "";
            stringList.Add("\t0x" + $"{palette_a8r8b8g8[i]:X8}" + tmpstr + "\t// '" + colorList[i].colorString + "'\r\n");
        }
        stringList.Add("};\r\n");

        stringList.Add("uint32_t palette_r8b8g8[" + palette_a8r8b8g8.Count + "] = \r\n{");

        for (int i = 0; i < palette_a8r8b8g8.Count; i++)
        {
            tmpstr = (i < palette_a8r8b8g8.Count - 1) ? "," : "";
            stringList.Add("\t0x" + $"{palette_a8r8b8g8[i]&0x00FFFFFF:X6}" + tmpstr + "\t// '" + colorList[i].colorString + "'\r\n");
        }
        stringList.Add("};\r\n");

        stringList.Add("uint16_t palette_r5g6b5[" + palette_r5b6g5.Count + "] = \r\n{");

        for (int i = 0; i < palette_r5b6g5.Count; i++)
        {
            tmpstr = (i < palette_r5b6g5.Count - 1) ? "," : "";
            stringList.Add("\t0x" + $"{palette_r5b6g5[i]:X4}" + tmpstr + "\t// '" + colorList[i].colorString + "'\r\n");
        }
        stringList.Add("};\r\n");
    }
}

/// <summary>
/// Ascii presentation <--> color hex code
/// </summary>
public class ColorTable
{
    public string colorString;
    public Color color;

    static ColorConverter converter = new ColorConverter();

    /// <summary>
    /// Initializes a new instance of the <see cref="ColorTable"/> class.
    /// </summary>
    /// <param name="colorAsString">The color code as string.</param>
    /// <param name="colorHexCode">The color's hexadecimal code.</param>
    public ColorTable(string colorAsString, string colorHexCode)
    {
        colorString = colorAsString;
        color = (Color)converter.ConvertFromString(colorHexCode);
    }
}

